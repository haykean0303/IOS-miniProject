//
//  DetailtViewController.swift
//  swifty-demo
//
//  Created by Mavin on 10/11/21.
//

import UIKit

class DetailtViewController: UIViewController {

    @IBOutlet weak var imageViewArti: UIImageView!
    @IBOutlet weak var titleViewArti: UILabel!
    @IBOutlet weak var desViewArti: UILabel!
    
    
    
    var article: Article?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        print("Title : ",article?.title ?? "")
        titleViewArti.text = article?.title
        desViewArti.text = article?.description
        let url = URL(string: article!.imageUrl)
                
        let defaultImage = UIImage(systemName: "camera.fill")
                
            self.imageViewArti.kf.setImage(with: url,placeholder: defaultImage, options: [.transition(.fade(0.25))])

    }
    

}
